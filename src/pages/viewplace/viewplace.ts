import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ResetpasswordPage } from '../index.paginas';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AngularFireDatabase } from 'angularfire2/database';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import * as firebase from 'firebase/app';
declare var google;

@IonicPage()
@Component({
  selector: 'page-viewplace',
  templateUrl: 'viewplace.html',
  /*template:
	`<ion-icon *ngFor="let n of ratingRange; let i = index" [name]="i < rating ? 'star' : 'star-outline'"></ion-icon>`,*/
})
export class ViewplacePage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  profile: any = {};
  user: any = {};
  session: boolean = false;
  testRadioOpen: boolean;
  testRadioResult;
  rate:any;
  rating:any;
  ratingRange:any;
  constructor(public navCtrl: NavController,
              private modalCtrl:ModalController,
              public navParams: NavParams,
              public afDb: AngularFireDatabase,
              public userProv: UserProvider,
              private plt: Platform,
              private geolocation: Geolocation,
              private platform: Platform,
              private androidPermissions: AndroidPermissions,
              public alerCtrl: AlertController,
              private locationAccuracy: LocationAccuracy) {
      this.profile = navParams.get("place");
      this.getRateByPlace(this.profile.uid)
      .then((ratePlace:any)=>{
          this.rate = Math.round(ratePlace);
          this.rating = Math.round(ratePlace);
          this.ratingRange = [1, 2, 3, 4, 5];
      })
      .catch();
      this.userProv.loadUserStorage().then(()=>{
        if( this.userProv.session_login ){
          console.log("ses ok");
          this.user = (this.userProv.user_profile) || false;
          this.session = (this.userProv.session_login) || false;
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewplacePage');
  }

  evaluatePlace(){
    {
    let alert = this.alerCtrl.create();
    
    alert.addInput({
      type: 'radio',
      label: '5 estrellas',
      value: '5',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: '4 estrellas',
      value: '4'
    });

    alert.addInput({
      type: 'radio',
      label: '3 estrellas',
      value: '3'
    });

    alert.addInput({
      type: 'radio',
      label: '2 estrellas',
      value: '2'
    });

    alert.addInput({
      type: 'radio',
      label: '1 estrellas',
      value: '1'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.testRadioOpen = false;
        this.testRadioResult = data;
        this.setRateByUser( this.user.uid, this.profile.uid, data );
      }
    });

    alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }
  }

  setRateByUser( uid, pid, rate ){
    firebase.database().ref(`/rated/${pid}/${uid}`).update({uid:uid, rate:rate});
  }
  getRateByPlace(pid){
    let _rates = 0;
    return new Promise( (resolve, reject)=>{
      this.afDb.list(`/rated/${pid}`, ref => ref.orderByKey()).valueChanges()
          .subscribe( (rates:any)=>{

            if(rates.length == 0){
              console.log("sin registros");
              resolve(0);
              return;
            }
            let numRate = rates.length;
            for(let i = rates.length-1; i >= 0; i-- ){
              _rates += parseInt(rates[i].rate);
            }
            let prom = _rates / numRate;
            resolve(prom);
          });
    });
  }

  showRouteMap(address){
    if (this.platform.is('cordova')) {
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
                                                  this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
                                                  this.androidPermissions.PERMISSION.ACCESS_LOCATION_EXTRA_COMMANDS,
                                                  this.androidPermissions.PERMISSION.ACCESS_NETWORK_STATE]).then((success)=>{
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
        .then(
          () => {
            this.geolocation.getCurrentPosition({ timeout: 30000 }).then((pos) => {
             this.map = new google.maps.Map(this.mapElement.nativeElement, { zoom: 13,
                                                                             mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                                             mapTypeControl: false,
                                                                             streetViewControl: false,
                                                                             fullscreenControl: false
                                                                           });
             let directionsService = new google.maps.DirectionsService;
             let directionsDisplay = new google.maps.DirectionsRenderer;
             let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
             this.map.setCenter(latLng);
             this.map.setZoom(16);
             directionsDisplay.setMap(this.map);

               directionsService.route({
                 origin: pos.coords.latitude+", "+pos.coords.longitude,
                 destination: address,
                 travelMode: 'DRIVING'
               }, function(response, status) {
                 if (status === 'OK') {
                   directionsDisplay.setDirections(response);
                 } else {
                   window.alert('Directions request failed due to ' + status);
                 }
               });

            }).catch((error) => {
              alert(error);
            });
          },
          error => console.log('Error requesting location permissions', error)
        );
      }).catch((error) => {
          alert("The following error occurred: "+error);
      });
    }
    else {
      let nativeElement = this.mapElement.nativeElement;
      this.plt.ready().then(() => {
        let mapOptions = {
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          streetViewControl: false,
          fullscreenControl: false
        }

        this.map = new google.maps.Map(nativeElement, mapOptions);
        let directionsService = new google.maps.DirectionsService;
        let directionsDisplay = new google.maps.DirectionsRenderer;
        this.geolocation.getCurrentPosition({
                                              enableHighAccuracy: true,
                                              timeout: 5000,
                                              maximumAge: 0
                                            }).then(pos => {
          console.log('pos getting location', pos);
          let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
          this.map.setCenter(latLng);
          this.map.setZoom(16);
          directionsDisplay.setMap(this.map);

            directionsService.route({
              origin: pos.coords.latitude+", "+pos.coords.longitude,
              destination: address,
              travelMode: 'DRIVING'
            }, function(response, status) {
              if (status === 'OK') {
                directionsDisplay.setDirections(response);
              } else {
                window.alert('Directions request failed due to ' + status);
              }
            });
        }).catch((error) => {
          console.log('Error getting location', JSON.stringify(error));
        });
      });
    }


  }

}
