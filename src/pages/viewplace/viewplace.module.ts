import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewplacePage } from './viewplace';

@NgModule({
  declarations: [
    ViewplacePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewplacePage),
  ],
})
export class ViewplacePageModule {}
