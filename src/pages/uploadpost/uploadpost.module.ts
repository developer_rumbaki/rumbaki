import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadpostPage } from './uploadpost';

@NgModule({
  declarations: [
    UploadpostPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadpostPage),
  ],
})
export class UploadpostPageModule {}
