import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';

import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";
import { CargaEventosProvider } from "../../providers/carga-eventos/carga-eventos";


@IonicPage()
@Component({
  selector: 'page-uploadpost',
  templateUrl: 'uploadpost.html',
})
export class UploadpostPage {

  title:string = "";
  text:string = "";
  type_post:string ="";
  imagenPreview:string = "";
  imagen64:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private viewCtrl:ViewController,
              private camera: Camera,
              private imagePicker: ImagePicker,
              public cargaArchivo:CargaArchivoProvider,
              private cargaEvP:CargaEventosProvider
            /*private crop: Crop*/) {
        if( !this.imagenPreview ){
          this.imagenPreview = "assets/img/default-img.png";
        }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadpostPage');
    switch(this.navParams.get("type")){
      case 'camera':
            console.log("activar camara");
            this.mostrarCamara();
      break;
      case 'file':
            console.log("activar archivos");
            this.selectPicture();
      default:
      break;
    }
  }

  mostrarCamara(){
    //const popOptions : CameraPopoverOptions = { x: 300, y: 300, height: 658, width: 658, arrowDir: this.camera.PopoverArrowDirection.ARROW_UP };
    //var popOptions = new CameraPopoverOptions(300,300,100,100,this.camera.PopoverArrowDirection.ARROW_ANY);
    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.CAMERA,//PHOTOLIBRARY,SAVEDPHOTOALBUM
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 960,
      targetHeight: 960,
      /*popoverOptions: popOptions ,*/
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imagenPreview = 'data:image/jpeg;base64,' + imageData;
      this.imagen64 = imageData;

    }, (err) => {
      // Handle error
      console.log( "Error en camara", JSON.stringify(err) );
    });

  }

  selectPicture(){
    let options: ImagePickerOptions = {
      quality:70,
      outputType:1,
      maximumImagesCount:1
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        //console.log('Image URI: ' + results[i]);
        this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
        this.imagen64 = results[i];

      }
    }, (err) => {
        console.log( "Error en selector", JSON.stringify(err) );
     });
  }

  createPost(){
    let timeToShow:string = "";
    switch( this.type_post ){
      case "events":
        timeToShow = "temporaly";
      break;
      case "posts":
        timeToShow = "permanent";
      break;
      default:
        timeToShow = "permanent";
      break;
    }
    let archivo: NewPost = {
      uri_attachement: this.imagen64,
      text: this.text,
      title: this.title,
      created_at: "",
      show: timeToShow,
      type: this.type_post
    }
    if(this.type_post =="events"){
      this.cargaEvP.uploadImageFirebase( archivo ).then(
        ()=>this.closeModal(),
        (err)=>{
          console.log( "Error en subir evento", JSON.stringify(err));
        });
    }
    else{
      this.cargaArchivo.uploadImageFirebase( archivo ).then(
        ()=>this.closeModal(),
        (err)=>{
          console.log( "Error en subir post", JSON.stringify(err));
        });
    }

  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}

interface NewPost {
  created_at:string;
  attachements?:[object];
  uri_attachement?:string;
  user_name?:string;
  user_avatar?:string;
  user_id?:string;
  title?:string;
  subtitle?:string;
  post_type?:string;
  text?:string;
  is_liked?:number;
  comments?:number;
  updated_at?:string;
  show?:string;
  type?:string;
}
