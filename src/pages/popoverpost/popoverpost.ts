import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';

@IonicPage()
@Component({
  selector: 'page-popoverpost',
  templateUrl: 'popoverpost.html',
})
export class PopoverpostPage {

  postid:string ="";
  tipo:string ="";
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private cargaAP:CargaArchivoProvider) {
                this.postid = navParams.get("pid");
                this.tipo = navParams.get("tipo");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverpostPage');
  }
  eliminarPublicacion(){
    //this.cargaAP.eliminarPublicacion(this.tipo, this.postid)
      firebase.database().ref(`/${this.tipo}/${this.postid}`).remove()
      .then((data)=>{
        console.log(data);
      })
      .catch((err)=>{
        console.log(err)
      });
      this.navCtrl.pop();

  }

}
