import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverpostPage } from './popoverpost';

@NgModule({
  declarations: [
    PopoverpostPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverpostPage),
  ],
})
export class PopoverpostPageModule {}
