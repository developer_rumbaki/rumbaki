import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';

import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";
import { UserProvider } from '../../providers/user/user';
import { AjustesProvider } from '../../providers/ajustes/ajustes';
//validadores de campos
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from "@angular/forms";

import { TabsPage } from '../index.paginas';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  avatarPreview:string = "";
  backgroundPreview:string = "";
  imagen64Avatar:string;
  imagen64Background:string;
  rumbero:boolean = false;
  submitAttempt: boolean = false;

  //password:string ="";
  confirmpassword:string ="";
  public loading:Loading;

  user = {
    avatar : "",
    username : "",
    firstname : "",
    lastname : "",
    email : "",
    password:"",
    uid : "",
    name_place : "",
    address : "",
    description : "",
    request_support : false,
    enable : true,
    enable_support : false,
    enable_admin : false,
    key : "",
    provider : "email",
    groups : {
      group_admin:false,
      group_anonym:false,
      group_support:false,
      group_users:true
    },
    location:{
      lat : "",
      lng : "",
    }
  };


  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public ajustesProv: AjustesProvider,
    public userProv: UserProvider,
    private camera: Camera,
    private imagePicker: ImagePicker,
    public cargaArchivo:CargaArchivoProvider) {
      if( !this.avatarPreview ){
        this.avatarPreview = "assets/img/avatar-img.jpg";
        this.backgroundPreview = "assets/img/default-img.png";
      }
      this.signupForm = new FormGroup({
        username:new FormControl( '', [ Validators.required, Validators.minLength(2) ] ),
        firstname:new FormControl( '', [ Validators.required, Validators.minLength(2) ] ),
        lastname:new FormControl( '', Validators.required ),
        email: new FormControl( '', Validators.pattern( ".+\@.+\..+" ) ),
        password: new FormControl( '', Validators.required ),
        confirmPassword: new FormControl( '', Validators.required ),
        rumbero: new FormControl(),
        name_place: new FormControl(),
        address: new FormControl(),
        description: new FormControl(),
        request_support: new FormControl(),
      });

    }

    showAlert( type, msg ) {
      const alert = this.alertCtrl.create({
        title: type,
        subTitle: msg,
        buttons: ['OK']
      });
      alert.present();
    }

    createUser() {
      if( this.user.username == "" ){
        this.showAlert("Atención","username requerido");
        return false;
      }
      if( this.user.firstname == "" ){
        this.showAlert("Atención","Nombre requerido");
        return false;
      }
      if( this.user.lastname == "" ){
        this.showAlert("Atención","Apellido requerido");
        return false;
      }
      if( this.user.password == "" ){
        this.showAlert("Atención","Contraseña requerido");
        return false;
      }else{
        if( this.confirmpassword == "" || this.confirmpassword != this.user.password ){
          this.showAlert("Atención","Confirmación de password no valido");
          return false;
        }
      }
      if(this.imagen64Avatar == "" || this.imagen64Avatar == undefined ){
        this.showAlert("Atención","Imagen de perfil requerido");
        return false;
      }
      else{
        this.user.avatar = this.imagen64Avatar;
      }
      if(this.rumbero){
        this.user.request_support = true;

      }
      this.userProv.createNewUser( this.user ).then(
          (data)=>{
              console.log( "user", JSON.stringify(data))
              this.ajustesProv.ajustes.mostrar_slides = false;
              this.ajustesProv.guardarStorage();
              this.navCtrl.setRoot(TabsPage);
          },
          (err)=>{
            console.log( "Error en crear user", JSON.stringify(err))
            //this.showAlert("Error","No se pudo crear el usuario");
            this.loading.dismiss().then( () => {
                let alert = this.alertCtrl.create({
                  message: err.message,
                  buttons: [
                    {
                      text: "Ok",
                      role: 'cancel'
                    }
                  ]
                });
                alert.present();
              });
          });
        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: true,
        });
        this.loading.present();
      console.log(JSON.stringify(this.user));
      /*let archivo: NewPost = {
      uri_attachement: this.imagen64,
      text: this.text,
      title: this.title,
      created_at: "",
      show: timeToShow,
      type: this.type_post
    }

    this.cargaArchivo.uploadImageFirebase( archivo ).then(
    ()=>{ console.log( "nuevo usuario") },
    (err)=>{
    console.log( "Error en subir post", JSON.stringify(err))
  });*/
}

selectPicture(option){
  let options: ImagePickerOptions = {
    quality:70,
    outputType:1,
    maximumImagesCount:1
  };
  this.imagePicker.getPictures(options).then((results) => {
    for (var i = 0; i < results.length; i++) {
      //console.log('Image URI: ' + results[i]);
      if( option == "avatar"){
        this.avatarPreview = 'data:image/jpeg;base64,' + results[i];
        this.imagen64Avatar = results[i];
      }else{
        this.backgroundPreview = 'data:image/jpeg;base64,' + results[i];
        this.imagen64Background = results[i];
      }
    }
  }, (err) => {
    console.log( "Error en selector", JSON.stringify(err) );
  });
}

mostrarCamara(option){
  const options: CameraOptions = {
    quality: 75,
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType : this.camera.PictureSourceType.CAMERA,//PHOTOLIBRARY,SAVEDPHOTOALBUM
    allowEdit : true,
    encodingType: this.camera.EncodingType.JPEG,
    targetWidth: 960,
    targetHeight: 960,
    /*popoverOptions: popOptions ,*/
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: false
  }

  this.camera.getPicture(options).then((imageData) => {
    if( option == "avatar"){
      this.avatarPreview = 'data:image/jpeg;base64,' + imageData;
      this.imagen64Avatar = imageData;
    }else{
      this.backgroundPreview = 'data:image/jpeg;base64,' + imageData;
      this.imagen64Background = imageData;
    }


  }, (err) => {
    // Handle error
    console.log( "Error en camara", JSON.stringify(err) );
  });
}

}
