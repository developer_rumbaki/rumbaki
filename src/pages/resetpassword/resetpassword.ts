import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from "@angular/forms";

import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {

  oldpassword:string ="";
  password:string ="";
  confirmpassword:string ="";
  changePasswordForm: FormGroup;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userProv: UserProvider,) {
    this.changePasswordForm = new FormGroup({
      oldpassword: new FormControl( '', Validators.required ),
      password: new FormControl( '', Validators.required ),
      confirmPassword: new FormControl( '', Validators.required ),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

  resetPassword(){
    this.userProv.changePassword(this.oldpassword, this.password)
      .then(()=>console.log("ok-*-*-*-*"))
      .catch((err)=>console.log(JSON.stringify(err)));
  }

}
