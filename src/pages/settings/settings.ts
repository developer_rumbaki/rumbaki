import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Loading  } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { AjustesProvider } from '../../providers/ajustes/ajustes';

import { LoginPage, AboutPage, RequestPage } from '../index.paginas';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  items:any = [ "item a", "item b", "item c", "item d", "item e" ];
  about:any = AboutPage;
  request:any = RequestPage;
  login:any = LoginPage;
  showItems:boolean = false;
  showAdmin:boolean = false;
  public loading:Loading;
  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private modalCtrl:ModalController,
    public navParams: NavParams,
    public ajustesProv: AjustesProvider,
    public userProv: UserProvider) {
      this.userProv.loadUserStorage().then(()=>{
        if( this.userProv.session_login ){
          console.log("valida ajustes");
          this.showItems = true;
          this.showAdmin = ( this.userProv && this.userProv.session_admin ) || false;
        }
      });

    }

    ionViewDidLoad(){
      this.userProv.loadUserStorage().then(()=>{
        if( this.userProv.session_login ){
          console.log("valida ajustes");
          this.showItems = true;
          this.showAdmin = ( this.userProv && this.userProv.session_admin ) || false;
        }
      });
    }

    goLogin(){
      let modal = this.modalCtrl.create( LoginPage );
      modal.present();
    }
    logOut(){
      this.userProv.logOut().then(()=>{
        this.loading.dismiss().then( () => {
          this.ajustesProv.ajustes.mostrar_slides = false;
          this.ajustesProv.ajustes.provider = "";
          this.ajustesProv.ajustes.session = false;
          this.userProv.session_login = false;
          this.ajustesProv.guardarStorage();
          let modal = this.modalCtrl.create( LoginPage );
          modal.present();
          //this.navCtrl.setRoot(LoginPage);
        });
      }, (err)=>{

      });
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();

    }

  }
