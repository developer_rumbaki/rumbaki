import { Component } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { UserProvider } from '../../providers/user/user';
import { AjustesProvider } from '../../providers/ajustes/ajustes';
import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";


@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
  user:any = {};
  signupForm: FormGroup;
  avatarPreview:string = "";
  imagen64Avatar:string = "";
  submitAttempt: boolean = false;
  public loading:Loading;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public ajustesProv: AjustesProvider,
    public userProv: UserProvider,
    private camera: Camera,
    private imagePicker: ImagePicker,) {
    this.user = navParams.get("profile");
    if( !this.user.avatar ){
      this.avatarPreview = "assets/img/avatar-img.jpg";
    }else{
      this.avatarPreview = this.user.avatar;
    }
    this.signupForm = new FormGroup({
      username:new FormControl( '', [ Validators.required, Validators.minLength(2) ] ),
      firstname:new FormControl( '', [ Validators.required, Validators.minLength(2) ] ),
      lastname:new FormControl( '', Validators.required ),
      email: new FormControl( '', Validators.pattern( ".+\@.+\..+" ) ),
      name_place: new FormControl(),
      address: new FormControl(),
      description: new FormControl(),
      request_support: new FormControl(),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

  mostrarCamara(option){
    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.CAMERA,//PHOTOLIBRARY,SAVEDPHOTOALBUM
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 960,
      targetHeight: 960,
      /*popoverOptions: popOptions ,*/
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
        this.avatarPreview = 'data:image/jpeg;base64,' + imageData;
        this.imagen64Avatar = imageData;

    }, (err) => {
      // Handle error
      console.log( "Error en camara", JSON.stringify(err) );
    });
  }

  selectPicture(option){
    let options: ImagePickerOptions = {
      quality:70,
      outputType:1,
      maximumImagesCount:1
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
          this.avatarPreview = 'data:image/jpeg;base64,' + results[i];
          this.imagen64Avatar = results[i];
      }
    }, (err) => {
      console.log( "Error en selector", JSON.stringify(err) );
    });
  }

  updateUser() {
    let imgUpd = false;
    if( this.user.username == "" ){
      this.showAlert("Atención","username requerido");
      return false;
    }
    if( this.user.firstname == "" ){
      this.showAlert("Atención","Nombre requerido");
      return false;
    }
    if( this.user.lastname == "" ){
      this.showAlert("Atención","Apellido requerido");
      return false;
    }
    if(this.avatarPreview == "" || this.avatarPreview == "assets/img/avatar-img.jpg" ){
      this.showAlert("Atención","Imagen de perfil requerido");
      return false;
    }
    else{
      if( this.imagen64Avatar.length > 1 ){
        imgUpd = true;
        this.user.avatar = this.imagen64Avatar;
      }
      else{
        this.user.avatar;
      }
    }
    this.userProv.updateUser( this.user, imgUpd ).then(
        (data)=>{
            console.log( "user", JSON.stringify(data))
            this.ajustesProv.ajustes.mostrar_slides = false;
            this.ajustesProv.guardarStorage();
            this.navCtrl.pop();
        },
        (err)=>{
          console.log( "Error en crear user", JSON.stringify(err))
          //this.showAlert("Error","No se pudo crear el usuario");
          this.loading.dismiss().then( () => {
              let alert = this.alertCtrl.create({
                message: err.message,
                buttons: [
                  {
                    text: "Ok",
                    role: 'cancel'
                  }
                ]
              });
              alert.present();
            });
        });
      this.loading = this.loadingCtrl.create({
        /*dismissOnPageChange: true,*/
        content: "Espere por favor...",
      duration: 500
      });
      this.loading.present();
    console.log(JSON.stringify(this.user));
  }

  showAlert( type, msg ) {
    const alert = this.alertCtrl.create({
      title: type,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
