import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';

import { ResetpasswordPage, EditprofilePage } from '../index.paginas';
import { Geolocation } from '@ionic-native/geolocation';

import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
declare var google;

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  session: boolean = false;
  profile: any = {};
  origin:string;
  constructor(public navCtrl: NavController,
              private modalCtrl:ModalController,
              public navParams: NavParams,
              public userProv: UserProvider,
              private plt: Platform,
              private geolocation: Geolocation,
              private androidPermissions: AndroidPermissions,
              private locationAccuracy: LocationAccuracy
            ) {

    this.userProv.loadUserStorage().then(()=>{
      console.log("this.userProv-31-");
      console.log(JSON.stringify(this.userProv.user_profile));
      if( this.userProv.session_login ){
        console.log("ses ok");
        this.profile = (this.userProv.user_profile) || false;
        this.session = (this.userProv.session_login) || false;

      }

    });
  }

  resetPassword() {
    const modal = this.modalCtrl.create(ResetpasswordPage);
    modal.present();
  }

  editProfile(profile){
    console.log("editar perfil");
    const modal = this.modalCtrl.create(EditprofilePage, { profile:profile });
    modal.present();
  }





}
