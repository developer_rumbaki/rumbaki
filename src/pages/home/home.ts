import { Component } from '@angular/core';
import { NavController, ModalController, PopoverController, AlertController  } from 'ionic-angular';

import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";
import { UserProvider } from '../../providers/user/user';

import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
//import { AngularFireDatabase } from 'angularfire2/database';
//firebase
//import { Observable } from 'rxjs';
//import * as firebase from "firebase";
//paginas
//forma de acceder a paginas que esten en los tabs
//this.navCtrl.parent.select(0);
import { ViewpostPage, UploadpostPage, EventsPage, CommentsPage, PopoverpostPage, ViewplacePage } from '../index.paginas';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //paginas
  viewpostPage: any = ViewpostPage;
  cargarPosts:boolean = true;
  noticias: string = "promos";
  canPost:boolean = false;
  useridStore:any;

  constructor(public navCtrl: NavController,
            private modalCtrl:ModalController,
            /*private afDB: AngularFireDatabase,*/
            private userProv:UserProvider,
            private cargaAP:CargaArchivoProvider,
            public popoverCtrl: PopoverController,
            public alertCtrl: AlertController) {
      this.userProv.loadUserStorage().then(()=>{
        if( this.userProv.session_login ){
          this.canPost = (this.userProv.user_profile && this.userProv.user_profile.enable_support) || false;
          this.useridStore = (this.userProv.user_profile && this.userProv.user_profile.uid) || "";
        }
      });
  }

  ionViewDidLoad(){

    this.userProv.loadUserStorage().then(()=>{
      if( this.userProv.session_login ){
        this.canPost = (this.userProv.user_profile && this.userProv.user_profile.enable_support) || false;
      }
    });
  }

  presentPopover(pid) {
    const popover = this.popoverCtrl.create(PopoverpostPage,{"pid":pid, "tipo":"posts"});
    popover.present();
  }

  presentAlert(post, index) {
    const confirm = this.alertCtrl.create({
      message: 'Desea borrar el post?',
      buttons: [
        {
          text: 'cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cargaAP.posts.splice(index,1);
            firebase.database().ref(`/posts/${post.key}`).remove()
            .then((data)=>{
            })
            .catch((err)=>{
              console.log(err)
            });
          }
        }
      ]
    });
    confirm.present();
  }

  uploadPost(type){
    let modal = this.modalCtrl.create( UploadpostPage, { "type":type } );
    modal.present();
  }
  viewPost( post ){
    //this.navCtrl.push(ViewpostPage, { "post": post });
    let modal = this.modalCtrl.create( ViewpostPage, { "post": post } );
    modal.present();
  }
  viewComments( pid ){
    let modal = this.modalCtrl.create( CommentsPage, { "pid": pid } );
    modal.present();
    modal.onDidDismiss( parametros =>{
      if( parametros )
        console.log(parametros);
    } );
  }
  viewLikes( pid ){
    this.modalCtrl.create( ViewpostPage, { "pid": pid } ).present();
  }

  segmentChanged( event ){
    switch( event._value ){
      case "events":
        this.navCtrl.setRoot( EventsPage );
      break;
      default:
      break;
      /*case "promos":
        this.navCtrl.setRoot( EventsPage );
      break;*/
    }
    //console.log( event );
    //firebase.auth().signInAnonymously();

  }

  doInfinite(infiniteScroll) {
    this.cargaAP.cargarPosts().then(
      ( cargarPosts: boolean )=>{
        this.cargarPosts = cargarPosts;
        infiniteScroll.complete();
      }
    ).catch((err)=>{
      console.log("Error : "+ JSON.stringify(err));
    });

  }

  viewPlace(uid){
    this.userProv.searchUser(uid).then((data)=>{
      let modal = this.modalCtrl.create( ViewplacePage, { "place": data } );
      modal.present();
    }).catch((err)=>{
      alert("El lugar ha sido desabilitado o ya no existe");
    })
  }

  viewEvents(){
    this.navCtrl.setRoot( EventsPage );
  }

}
