import { Component, ViewChild  } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, Content } from 'ionic-angular';
import { CargaLugaresProvider } from "../../providers/carga-lugares/carga-lugares";
import { ViewplacePage } from '../index.paginas';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

@IonicPage()
@Component({
  selector: 'page-places',
  templateUrl: 'places.html',
})
export class PlacesPage {
  viewPlace: any = ViewplacePage;
  loadPlaces:boolean = true;
  places:any;
  searchPlaces:string ="";
  searchControl: FormControl;
  searching: any = false;
  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController,
    private modalCtrl:ModalController,
    public navParams: NavParams,
    private placesProv:CargaLugaresProvider) {
      //this.mutante = navParams.get("mutante");

      this.placesProv.cargarTodosLugares().then((data)=>{
        if(data)
        this.places = data;
      }).catch((err)=>{
        this.places = [];
      });
      this.searchControl = new FormControl();
    }
    ionViewDidLoad() {
      this.setFilteredItems();
      this.searchControl.valueChanges.debounceTime(700).subscribe(search  => {
        this.setFilteredItems();
      });
    }

    ionViewDidEnter(){
      this.placesProv.cargarTodosLugares().then((data)=>{
        if(data)
        this.places = data;
      }).catch((err)=>{
        this.places = [];
      });
      console.log("ionViewDidEnter");
    }
    viewPlaceInfo(place){
      let modal = this.modalCtrl.create( ViewplacePage, { "place":place } );
      modal.present();
    }

    onSearchInput(){
      this.searching = true;
    }

    setFilteredItems() {
      this.places = this.placesProv.filterItems(this.searchPlaces);
    }

    doInfinite(infiniteScroll) {
      console.log('Begin async operation');
      this.placesProv.cargarLugares().then(
        ( loadPlaces: boolean )=>{
          console.log(loadPlaces);
          this.loadPlaces = loadPlaces;
          infiniteScroll.complete();
        }
      );
    }




  }
