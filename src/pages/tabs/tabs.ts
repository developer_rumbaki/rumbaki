import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage, ProfilePage, UploadpostPage, PlacesPage, SettingsPage } from '../index.paginas';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tabHome:any;
  tabProfile:any;
  tabUpload:any;
  tabPlaces:any;
  tabSettings:any;
  showUpload:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabHome = HomePage;
    this.tabProfile = ProfilePage;
    this.tabUpload = UploadpostPage;
    this.tabPlaces = PlacesPage;
    this.tabSettings = SettingsPage;

  }
  /*
  ionViewCanEnter(){
    console.log("ionViewCanEnter");

    let promesa = new Promise( (resolve, reject)=>{

      resolve(true);
    });

    return promesa;

  }*/

}
