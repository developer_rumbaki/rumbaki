import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-viewpost',
  templateUrl: 'viewpost.html',
})
export class ViewpostPage {

  post:any = {};
  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.post = navParams.get("post");
    console.log(navParams);
    //console.log(JSON.stringify(this.post));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewpostPage');
  }

}
