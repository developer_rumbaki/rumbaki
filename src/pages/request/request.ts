import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CargaSolicitudesProvider } from "../../providers/carga-solicitudes/carga-solicitudes";


@IonicPage()
@Component({
  selector: 'page-request',
  templateUrl: 'request.html',
})
export class RequestPage {

  cargarUsers:boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadRequestProv: CargaSolicitudesProvider) {
    }

    updateUserToSupport(user){
      user.enable_support = user.enable_support;
      user.enable_admin = user.enable_admin;
      this.loadRequestProv.habilitaUsuario(user);
    }
    /*
    doInfinite(infiniteScroll) {
    this.loadRquestProv.cargarSolicitudes().then(
    ( cargarSolicitudes: boolean )=>{
    console.log(cargarSolicitudes);
    this.cargarUsers = cargarSolicitudes;
    infiniteScroll.complete();
  }
);
}
*/

}
