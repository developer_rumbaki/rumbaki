import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TabsPage, SignupPage } from '../index.paginas';

import { AjustesProvider } from '../../providers/ajustes/ajustes';
import { UserProvider } from '../../providers/user/user';

import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
import 'firebase/auth';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email:string ="";
  password:string="";
  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public navParams: NavParams,
              public ajustesProv: AjustesProvider,
              public userProv: UserProvider,
              private fb: Facebook,
              private platform: Platform,
              public storage: Storage,
              private afAuth: AngularFireAuth) {
  }

  login(){
    let strorage = this.storage;
    this.userProv.loginUser(this.email, this.password ).then( (user)=>{
      this.ajustesProv.ajustes.mostrar_slides = false;
      this.ajustesProv.ajustes.provider = "email";
      this.ajustesProv.guardarStorage();
      console.log("user----------");
      console.log(user);
      //this.userProv.setUserByProvider( user );
      //this.userProv.saveUserStorage();
      this.navCtrl.setRoot(TabsPage);
    },(error)=>{
      this.showAlert( error.message );
    });
  }
  login2(){
    let platform = this.platform.is( "cordova" );
    let strorage = this.storage;
    let _navCtrl = this.navCtrl;
    this.userProv.loginUser2(this.email, this.password ).then( (user:any)=>{
      if (user) {
        firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
          if( platform ){//dispositivo
            strorage.ready().then( ()=>{
                strorage.set( "user", { "profile" : snapshot.val(), "session":true });
                strorage.set( "ajustes", { mostrar_slides:false, provider:"email" });
              });
            }else{//escritorio
              localStorage.setItem( "user",  JSON.stringify( { "profile" : snapshot.val(), "session":true } ) );
              localStorage.setItem( "ajustes",  JSON.stringify( { mostrar_slides:false, provider:"email" } ) );
            }
            _navCtrl.setRoot(TabsPage);
        })
        .catch(()=>{
          this.showAlert( "El usuario a sido borrado" );
        });
      }
    },(error)=>{
      this.showAlert( error.message );
    });
  }

  showAlert( msg ) {
    const alert = this.alertCtrl.create({
      message: msg,
      buttons: [
        {
          text: "Ok",
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }
 /*
  signInWithGoogle(){
    let _navCtrl = this.navCtrl;
    this.googlePlus.login({
      'webClientId': '740720861556-kigk925igta3o978fen8u0hv5q7ql7qq.apps.googleusercontent.com',
      'offline': true
    }).then( res => {
      console.log(res)
      firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
      .then( (user) => {
        console.log("Firebase success: " + JSON.stringify(user));
        //
          let _newUser: UserApp = { avatar : user.photoURL, username : user.displayName, firstname : user.displayName,
                                    lastname : "",          email : user.email,          key : user.uid,
                                    uid : user.uid,         provider : "google",       enable : true,
                                    groups : {
                                      group_admin:false,
                                      group_anonym:false,
                                      group_support:false,
                                      group_users:true
                                    },                     name_place : "",              address : "",
                                    description : "",      request_support : false,      enable_support : false,
                                    enable_admin : false,  location:{ lat:"", lng:"" }
                                  };
          localStorage.setItem( "user",  JSON.stringify( { "profile" : _newUser, "session":true } ) );
          firebase.database().ref(`/users/${_newUser.uid}`).update(_newUser).then(
            ()=>{
              console.log("Usuario creado--!")
            },
            (err)=>{
              console.log("se subio mal..--.....");
            });
        localStorage.setItem( "ajustes", JSON.stringify( { mostrar_slides:false, provider:"facebook", session:true, uid:"", key:"" } ) );
        _navCtrl.setRoot(TabsPage);
        //
      });
    }).catch(err => {
      console.error("err:--------------"+JSON.stringify(err))
    });
  }*/

  signInWithFacebook() {
    let _navCtrl = this.navCtrl;
    let strorage = this.storage;
    if (this.platform.is('cordova')) {
          this.fb.login(['email', 'public_profile']).then(res => {
            const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
            firebase.auth().signInWithCredential(facebookCredential).then( user => {
                console.warn("Usuario creado--!"+JSON.stringify(user));
              firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                if(snapshot.val()){
                  strorage.ready().then( ()=>{
                      strorage.set( "user", { "profile" : snapshot.val(), "session":true });
                    });
                }else{
                  let _newUser: UserApp = { avatar : user.photoURL, username : user.displayName, firstname : user.displayName,
                                            lastname : "",          email : user.email,          key : user.uid,
                                            uid : user.uid,         provider : "facebook",       enable : true,
                                            groups : {
                                              group_admin:false,
                                              group_anonym:false,
                                              group_support:false,
                                              group_users:true
                                            },                     name_place : "",              address : "",
                                            description : "",      request_support : false,      enable_support : false,
                                            enable_admin : false,  location:{ lat:"", lng:"" }
                                          };
                  strorage.ready().then( ()=>{
                      strorage.set( "user", { "profile" : _newUser, "session":true });
                    });
                  firebase.database().ref(`/users/${_newUser.uid}`).update(_newUser).then(
                    ()=>{
                      console.log("Usuario creado--!")
                    },
                    (err)=>{
                      console.log("se subio mal..--.....");
                    });
                }
                strorage.ready().then( ()=>{
                    strorage.set( "ajustes", { mostrar_slides:false, provider:"facebook", session:true, uid:"", key:"" });
                  });
                _navCtrl.setRoot(TabsPage);
              });

            }).catch( err => {
                console.log(JSON.stringify( err ));
            })
          });
        }
        else {
          this.afAuth.auth
            .signInWithPopup(new firebase.auth.FacebookAuthProvider())
            .then(res => {
              let usuario = res.user;
              firebase.database().ref('/users/' + usuario.uid).once('value').then(function(snapshot) {
                if(snapshot.val()){
                  localStorage.setItem( "user",  JSON.stringify( { "profile" : snapshot.val(), "session":true } ) );
                  localStorage.setItem( "ajustes", JSON.stringify( { mostrar_slides:false, provider:"facebook", session:true, uid:"", key:"" } ) );
                }else{
                  let _newUser: UserApp = {
                                            avatar : usuario.photoURL,
                                            username : usuario.displayName,
                                            firstname : usuario.displayName,
                                            lastname : usuario.displayName,
                                            email : usuario.email,
                                            key : usuario.uid,
                                            uid : usuario.uid,
                                            provider : "facebook",
                                            enable : true,
                                            groups : {
                                                        group_admin:false,
                                                        group_anonym:false,
                                                        group_support:false,
                                                        group_users:true
                                                      },
                                            name_place : "",
                                            address : "",
                                            description : "",
                                            request_support : false,
                                            enable_support : false,
                                            enable_admin : false,
                                            location:{
                                              lat:"",
                                              lng:"",
                                            }
                                          };
                  localStorage.setItem( "user",  JSON.stringify( { "profile" : _newUser, "session":true } ) );
                  localStorage.setItem( "ajustes", JSON.stringify( { mostrar_slides:false, provider:"facebook", session:true, uid:"", key:"" } ) );
                  firebase.database().ref(`/users/${_newUser.uid}`).update(_newUser).then(
                    ()=>{
                      console.log("Usuario creado--!")
                    },
                    (err)=>{
                      console.log("se subio mal..--.....");
                    });
                }
                _navCtrl.setRoot(TabsPage);
              });
              //this.navCtrl.setRoot(TabsPage);

              //this.userProv.createNewUser( _newUser );
              //this.userProv.verifySocialUser(_newUser);

            });
        }


  }

  signup(){
    this.navCtrl.push(SignupPage);
  }

  resetPassword(){
    this.userProv.resetPassword("email").then( ()=>{
      let alert = this.alertCtrl.create({
        message: "Te enviamos un link a tu correo.",
        buttons: [ {
                      text: "Ok",
                      role: 'cancel',
                      handler: () => {
                        //this.nav.pop();
                      }
                    }]
      });
      alert.present();
    }).catch( (error)=>{
      var errorMessage: string = error.message;
      let errorAlert = this.alertCtrl.create({
        message: errorMessage,
        buttons: [
          {
            text: "Ok",
            role: 'cancel'
          }
        ]
      });
      errorAlert.present();
    });
  }

  goToHome(){
    this.ajustesProv.ajustes.mostrar_slides = false;
    this.ajustesProv.guardarStorage();
    this.navCtrl.setRoot( TabsPage );
  }

}

export interface UserApp {
  avatar:string;
  username:string;
  firstname:string;
  lastname:string;
  email:string;
  groups: {
    group_admin:boolean;
    group_anonym:boolean;
    group_support:boolean;
    group_users:boolean;
  };
  background?:string;
  key?:string;
  password?:string;
  uid?:string;
  address?:string;
  name_place?:string;
  description?: "";
  url?:string;
  enable?:boolean;
  enable_support?:boolean;
  request_support?:boolean;
  enable_admin?:boolean;
  location?: {
    lat?:string;
    lng?:string;
  };
  created_at?:string;
  provider?:string;
}
