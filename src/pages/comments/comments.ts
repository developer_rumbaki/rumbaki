import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { CargaComentariosProvider } from '../../providers/carga-comentarios/carga-comentarios';

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  canPost:boolean = false;
  user_profile: any= {};
  comentario:string = "";
  pid:string = null;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProv:UserProvider,
              private comentProv: CargaComentariosProvider) {
              /*this.pid = this.navParams.get("pid");
              this.comentProv.loadCommentsFstime(this.pid);*/
              this.userProv.loadUserStorage().then(()=>{
                this.canPost = (this.userProv.user_profile && this.userProv.user_profile.enable_support) || false;
                this.user_profile = (this.userProv.user_profile) || { "avatar":"", "uid": "", "username":"" };
              });
              this.pid = this.navParams.get("pid");
              this.comentProv.loadCommentsFstime(this.pid);
  }
  /*
  ionViewDidLoad(){
    this.canPost = (this.userProv.user_profile && this.userProv.user_profile.enable_support) || false;
    this.user_profile = (this.userProv.user_profile) || { "avatar":"", "uid": "", "username":"" };
    this.pid = this.navParams.get("pid");
    this.comentProv.loadCommentsFstime(this.pid);
    console.log( "ionViewDidLoad --");
  }
 */
  /*ionViewWillEnter(){
    console.log("ionViewWillEnter");
    this.comentProv.loadCommentsFstime(this.navParams.get("pid"));
  }*/

  sendComment(){
    this.comentProv.newComment( this.pid, this.user_profile.uid, this.comentario, this.user_profile.avatar, this.user_profile.username  );
    this.comentario = "";
  }

}
