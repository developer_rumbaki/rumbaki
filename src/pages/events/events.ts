import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { ViewpostPage, UploadpostPage, HomePage, CommentsPage, ViewplacePage } from '../index.paginas';
import { CargaEventosProvider } from "../../providers/carga-eventos/carga-eventos";
import { UserProvider } from '../../providers/user/user';

import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  viewpostPage: any = ViewpostPage;
  cargarEvents:boolean = true;
  noticias: string = "events";
  canPost:boolean = false;
  useridStore:any;
  constructor(public navCtrl: NavController,
            private modalCtrl:ModalController,
            private userProv:UserProvider,
            public alertCtrl: AlertController,
            private cargaEvP:CargaEventosProvider ) {


  }

  ionViewDidLoad(){
    this.canPost = (this.userProv.user_profile && this.userProv.user_profile.enable_support) || false;
    this.useridStore = (this.userProv.user_profile && this.userProv.user_profile.uid) || "";
  }

  presentAlert(post, index) {
    const confirm = this.alertCtrl.create({
      message: 'Desea borrar el evento?',
      buttons: [
        {
          text: 'cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cargaEvP.events.splice(index,1);
            firebase.database().ref(`/events/${post.key}`).remove()
            .then((data)=>{
              console.log("data");
              console.log(data);
            })
            .catch((err)=>{
              console.log(err)
            });
          }
        }
      ]
    });
    confirm.present();
  }

  uploadPost(type){
    let modal = this.modalCtrl.create( UploadpostPage, { "type":type } );
    modal.present();
  }
  viewPost( post ){
    //this.navCtrl.push(ViewpostPage, { "post": post });
    let modal = this.modalCtrl.create( ViewpostPage, { "post": post } );
    modal.present();
  }

  viewComments( pid ){
    let modal = this.modalCtrl.create( CommentsPage, { "pid": pid } );
    modal.present();
    modal.onDidDismiss( parametros =>{
      if( parametros )
        console.log(parametros);
    } );
  }

  doInfinite(infiniteScroll) {
    this.cargaEvP.cargarEvents().then(
      ( cargarEvents: boolean )=>{
        this.cargarEvents = cargarEvents;
        infiniteScroll.complete();
      }
    ).catch((err)=>{
      console.log("Error infiniti event: "+ JSON.stringify(err));
    });

  }

  segmentChanged( event ){
    console.log( event._value );
    switch( event._value ){
      case "promos":
        this.navCtrl.setRoot( HomePage );
      break;
      default:
      break;
    }
  }

  viewPosts(){
    this.navCtrl.setRoot( HomePage );
  }

  viewPlace(uid){
    this.userProv.searchUser(uid).then((data)=>{
      let modal = this.modalCtrl.create( ViewplacePage, { "place": data } );
      modal.present();
    }).catch((err)=>{
      alert("El lugar ha sido desabilitado o ya no existe");
    })
  }

}
