import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage, LoginPage } from '../index.paginas';
import { AjustesProvider } from '../../providers/ajustes/ajustes';
//firebase
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-introduction',
  templateUrl: 'introduction.html',
})
export class IntroductionPage {

  slides:any[] = [
    {
      title: "Bienvenido!!!",
      description: "Si buscas donde farrear, comer o pasar el rato <strong>Rumbaki</strong> te ayudará a elegir!",
      image: "assets/img/rumbaki-logotipo.png",
      color: "#272262",
      gradient: "gradient2",
    },
    {
      title: " ",
      description: "Aquí encontraras los mejores lugares para: escuchar musica en vivo, conciertos, discotecas y bares.",
      image: "assets/img/band-img.png",
      color: "#6858EB",
      gradient: "gradient2",
    },
    {
      title: " ",
      description: "Tambien tendras restaurantes, o bares donde sirvan la mejor comida.",
      image: "assets/img/eat-beer-img.png",
      color: "#FD3F2F",
      gradient: "gradient1",
    }
  ];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private ajustesProv: AjustesProvider) {
  }
  /*
  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroductionPage');
    if( !this.ajustesProv.ajustes.mostrar_slides ){
      console.log('ionViewCanEnter IntroductionPage');
      this.navCtrl.setRoot( LoginPage );
    }
  }*/
  ionViewCanEnter(){
    if( !this.ajustesProv.ajustes.mostrar_slides ){
      console.log('ionViewCanEnter IntroductionPage');
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in
          console.log( "userAUTH" );
          console.log( user );
          console.log("ya estaba iniciada intro");
          //let navCtrlPartial: NavController;
          //this.navCtrlPartial.setRoot(TabsPage);
          this.navCtrl.setRoot(TabsPage);
        }
        else{
          this.navCtrl.setRoot( LoginPage );
        }
      });

    }
  }

  saltar_tutorial(){
    this.ajustesProv.ajustes.mostrar_slides = false;
    this.ajustesProv.guardarStorage();
    this.navCtrl.setRoot( LoginPage );
    //this.navCtrl.setRoot( TabsPage );
  }
  goToHome(){
    this.ajustesProv.ajustes.mostrar_slides = false;
    this.ajustesProv.guardarStorage();
    //this.navCtrl.setRoot( TabsPage );
    this.navCtrl.setRoot( LoginPage );
  }

}
