import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SplashfirstPage } from './splashfirst';

@NgModule({
  declarations: [
    SplashfirstPage,
  ],
  imports: [
    IonicPageModule.forChild(SplashfirstPage),
  ],
})
export class SplashfirstPageModule {}
