import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
//lista de paginas
import { HomePage, TabsPage, ProfilePage, IntroductionPage,
         LoginPage, UploadpostPage, ViewpostPage, PlacesPage, ResetpasswordPage,
         SettingsPage, EventsPage, SignupPage, AboutPage, RequestPage, CommentsPage,
         PopoverpostPage, EditprofilePage, ViewplacePage} from '../pages/index.paginas';
//firebaseConfig
import { firebaseConfig } from '../config/firebase.config';
//providers
import { AjustesProvider } from '../providers/ajustes/ajustes';
import { CargaArchivoProvider } from '../providers/carga-archivo/carga-archivo';
import { UserProvider } from '../providers/user/user';
import { CargaUsersProvider } from '../providers/carga-users/carga-users';
import { CargaLugaresProvider } from '../providers/carga-lugares/carga-lugares';
//pluggins
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { Facebook } from '@ionic-native/facebook';

import { HttpModule } from '@angular/http';
import { CargaSolicitudesProvider } from '../providers/carga-solicitudes/carga-solicitudes';
import { CargaEventosProvider } from '../providers/carga-eventos/carga-eventos';
import { CargaComentariosProvider } from '../providers/carga-comentarios/carga-comentarios';

import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    ProfilePage,
    IntroductionPage,
    LoginPage,
    UploadpostPage,
    ViewpostPage,
    PlacesPage,
    SettingsPage,
    EventsPage,
    SignupPage,
    AboutPage,
    RequestPage,
    CommentsPage,
    ResetpasswordPage,
    PopoverpostPage,
    EditprofilePage,
    ViewplacePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    ProfilePage,
    IntroductionPage,
    LoginPage,
    UploadpostPage,
    ViewpostPage,
    PlacesPage,
    SettingsPage,
    EventsPage,
    SignupPage,
    AboutPage,
    RequestPage,
    CommentsPage,
    ResetpasswordPage,
    PopoverpostPage,
    EditprofilePage,
    ViewplacePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    ImagePicker,
    Crop,
    UserProvider,
    Facebook,
    Geolocation,
    AndroidPermissions,
    LocationAccuracy,
    AjustesProvider,
    CargaArchivoProvider,
    CargaUsersProvider,
    CargaLugaresProvider,
    CargaSolicitudesProvider,
    CargaEventosProvider,
    CargaComentariosProvider,
  ]
})
export class AppModule {}
