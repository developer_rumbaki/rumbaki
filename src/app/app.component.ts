import { Component } from '@angular/core';
import { Platform/*, MenuController*/ } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//lista de paginas
import { TabsPage, HomePage, ProfilePage, IntroductionPage, LoginPage } from '../pages/index.paginas';
//plugins
import { AjustesProvider } from '../providers/ajustes/ajustes';
import { UserProvider } from '../providers/user/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  //rootPage:any = TabsPage;
  rootPage:any = IntroductionPage;
  tabs = TabsPage;
  home = HomePage;
  profile = ProfilePage;

  constructor(private platform: Platform, statusBar: StatusBar,
              splashScreen: SplashScreen,
              /*private menuCtrl: MenuController,*/
              private ajustesProv: AjustesProvider,
              private userProvider: UserProvider) {
    platform.ready().then(() => {
      this.ajustesProv.cargarStorage()
        .then( ()=>{
          this.userProvider.verifyAuth().then((data:any)=>{
            //console.error(this.userProvider);
            this.userProvider.saveUserStorage( data.profile, data.session );
            this.userProvider.loadUserStorage().then(()=>{
              if(this.userProvider.session_login){
                this.rootPage = TabsPage;
                statusBar.styleDefault();
                splashScreen.hide();
              }else{
                if( !this.ajustesProv.ajustes.mostrar_slides ){
                  this.rootPage = LoginPage;
                }else{
                  this.rootPage = IntroductionPage;
                }
                statusBar.styleDefault();
                splashScreen.hide();
              }

            });
          })
          .catch(()=>{

          });

        });

    });
  }


}
