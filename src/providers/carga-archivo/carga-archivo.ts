import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

//firebase
import { AngularFireDatabase } from 'angularfire2/database';
//import * as firebase from "firebase";
import   "rxjs/add/operator/map";
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
//import 'firebase/messaging';
import 'firebase/auth';


//import { firebaseConfig } from '../../config/firebase.config';
import { UserProvider } from '../../providers/user/user';

@Injectable()
export class CargaArchivoProvider {

  posts:NewPost[] = [];
  lastKey: string = null;
  constructor( public toastCtrl: ToastController,
               public afDb: AngularFireDatabase,
               public userProv: UserProvider ) {
    this.cargarUltimoKey()
        .subscribe( (post:any)=>{
          //if(post)
            this.cargarPosts().then((ok)=>{
                console.warn("ok: "+JSON.stringify(ok));
            }).catch((err)=>{
              console.warn("error: "+JSON.stringify(err));
            });
            this.posts = this.removeDuplicates(this.posts, "key");
         });
  }

  public eliminarPublicacion(tipo, pid){
    let pos = this.posts.indexOf(pid);
    console.log("pos");
    console.log(pos);
    firebase.database().ref(`/${tipo}/${pid}`).remove()
    .then((data)=>{
      console.log(data);

      this.posts.splice(pos,pid);
    })
    .catch((err)=>{
      console.log(err)
    });
  }

  private cargarUltimoKey(){
    return this.afDb.list('/posts', ref=> ref.orderByKey().limitToLast(1) )
            .valueChanges()
            .map( (post:any ) =>{
              try{
                this.lastKey = post[0].key;
                this.posts.push( post[0] );
                this.posts = this.removeDuplicates(this.posts, "key");
              }catch(err){
                  console.log(err);
              }
            });
  }

  cargarPosts(){
    return new Promise( (resolve, reject)=>{
      this.afDb.list('/posts', ref => ref.limitToLast(3)
          .orderByKey()
          .endAt(this.lastKey) ).valueChanges()
          .subscribe( (posts:any)=>{
            posts.pop();
            if(posts.length == 0){
              console.log("sin registros");
              resolve(false);
              return;
            }
            this.lastKey = posts[0].key;
            for(let i = posts.length-1; i >= 0; i-- ){
              let post = posts[i];
              this.posts.push(post);
            }
            this.posts = this.removeDuplicates(this.posts, "key");
            resolve(true);
          });
    });
  }

  uploadImageFirebase( archivo:NewPost ){
    let promesa = new Promise( ( resolve, reject )=>{
      this.showToast("Cargando....");
      let storeRef = firebase.storage().ref();
      let user_id:string = new Date().valueOf().toString();
      let uploadTask : firebase.storage.UploadTask =
      storeRef.child(`img/${user_id}`).putString( archivo.uri_attachement, 'base64', { contentType: 'image/jpeg'} );
      uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
        ()=>{ },//porcentaje de archivo subido
        (error)=>{
        //error de carga
        console.log( JSON.stringify( error ) );
        this.showToast( JSON.stringify( error ) );
        reject();
      },
      ( )=>{
        //todo bien
        this.showToast( "Promo creada exitosamente" );
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
           this.crearPost( user_id, archivo.title, archivo.text, downloadURL, archivo.type );
         });
         resolve();
        //console.log(url);


      }
    )


  });
    return promesa;
  }

  private crearPost( user_id:string, titulo:string, text:string, url:string, type:string ){
    let date = new Date();
    let keyattach = Math.random().toString(36).substring(7);
    let post: NewPost = {
      attachements: [{ key: keyattach, title:"", content: url}],
      title: titulo,
      text: text,
      key: user_id,
      user_id: this.userProv.user_profile.uid,
      created_at: date.getDate()+"/"+date.getMonth(),
      user_avatar : this.userProv.user_profile.avatar,
      user_name : this.userProv.user_profile.username,
      comments : 0,
      likes : 0
    };
    console.log(JSON.stringify(post));
    this.afDb.object(`/posts/${user_id}`).update(post).then(
      (ok)=> {
        console.log("promo ok: "+JSON.stringify(ok));
      } ,(err)=>{
      console.log("promo fallo: "+JSON.stringify(err));
    });
    //this.posts.push( post );
    this.posts.unshift( post );
    this.posts = this.removeDuplicates(this.posts, "key");
  }

  showToast( mensaje:string ){

    this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    }).present();

  }

  removeDuplicates(originalArray, prop) {
     let newArray = [];
     let lookupObject  = {};

     for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
     }

     for(i in lookupObject) {
         newArray.push(lookupObject[i]);
     }
      return newArray;
  }

}

interface NewPost {
  created_at:string;
  attachements?:[object];
  uri_attachement?:string;
  user_address?:string;
  user_name?:string;
  user_avatar?:string;
  user_id?:string;
  title?:string;
  subtitle?:string;
  post_type?:string;
  text?:string;
  is_liked?:number;
  comments?:number;
  likes?:number;
  updated_at?:string;
  show?:string;
  type?:string;
  key?:string;
}

//const rootRef = firebase.database().ref();
// select a user by UID ejemplo el id # 1
// const oneRef = rootRef.child('users').child('1');
//find a user by email
// const twoRef = rootRef.child('users').orderByChild('email').equalTo('jack@gmail.com');
//limit to 10 users
// const threeRef = rootRef.child('users').limitToFirst(10);
//get users name that start with 'D'
// const fourRef = rootRef.child('users').orderByChild('name').startAt('D').endAt('D\utf8ff');
//get users age less 50
// const fiveRef = rootRef.child('users').orderByChild('age').endAt(49);
//get users age greater 50
// const sixRef = rootRef.child('users').orderByChild('age').startAt(51);
//get users who are betwen 20 and 100
// const sevenRef = rootRef.child('users').orderByChild('age').startAt(20).endAt(100);
//get users who are betwen 28 and live in Berlin -- se unen columnas --
// const eightRef = rootRef.child('users').orderByChild('age_location').equalTo('28_Berlin');
