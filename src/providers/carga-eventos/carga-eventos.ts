import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

//firebase
import { AngularFireDatabase } from 'angularfire2/database';
//import * as firebase from "firebase";
import   "rxjs/add/operator/map";
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
import 'firebase/auth';


//import { firebaseConfig } from '../../config/firebase.config';
import { UserProvider } from '../../providers/user/user';

@Injectable()
export class CargaEventosProvider {

    events:NewEvents[] = [];
    lastKey: string = null;
    constructor( public toastCtrl: ToastController,
                 public afDb: AngularFireDatabase,
                 public userProv: UserProvider ) {
      this.cargarUltimoKey()
          .subscribe( (post:any)=>{
              console.log(post);
              this.cargarEvents()
              .then(()=>{})
              .catch((err)=>{console.warn(JSON.stringify(err));
              });
              this.events = this.removeDuplicates(this.events, "key");
           });
    }

    private cargarUltimoKey(){
      return this.afDb.list('/events', ref=> ref.orderByKey().limitToLast(1) )
              .valueChanges()
              .map( (event:any ) =>{
                try{
                  this.lastKey = event[0].key;
                  this.events.push( event[0] );
                  this.events = this.removeDuplicates(this.events, "key");
                }catch(err){
                    console.log(err);
                }
              });
    }

    cargarEvents(){
      return new Promise( (resolve, reject)=>{
        this.afDb.list('/events', ref => ref.limitToLast(3)
                        .orderByKey()
                        .endAt(this.lastKey) ).valueChanges()
                        .subscribe( (events:any)=>{
                          console.log(JSON.stringify(events));
                          events.pop();
                          if(events.length == 0){
                            console.log("sin registros");
                            resolve(false);
                            return;
                          }
                          this.lastKey = events[0].key;
                          for(let i = events.length-1; i >= 0; i-- ){
                            let event = events[i];
                            this.events.push(event);
                          }
                          this.events = this.removeDuplicates(this.events, "key");
                          resolve(true);
                        });
      });
    }

    uploadImageFirebase( archivo:NewEvents ){
      return new Promise( ( resolve, reject )=>{
        this.showToast("Cargando....");
        /*if(this.userProv.user_profile){

        }*/
        let storeRef = firebase.storage().ref();
        let user_id:string = new Date().valueOf().toString();
        //let user_id:string = this.getPostId();
        let uploadTask : firebase.storage.UploadTask =
        storeRef.child(`img/${user_id}`).putString( archivo.uri_attachement, 'base64', { contentType: 'image/jpeg'} );
        uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
          ()=>{ },//porcentaje de archivo subido
          (error)=>{
          //error de carga
          console.log( "Error en la carga" );
          this.showToast( JSON.stringify( error ) );
          reject();
        },
        ( )=>{
          //todo bien
          this.showToast( "Evento creado exitosamente!" );
          uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
             this.crearEvent( user_id, archivo.title, archivo.text, downloadURL, archivo.type );
           }).catch((error)=>{
             console.log("error al subir : "+JSON.stringify(error));
           });
          //console.log(url);
          resolve();
        });


    });
    }

    private crearEvent( user_id:string, titulo:string, text:string, url:string, type:string ){
      let date = new Date();
      let keyattach = Math.random().toString(36).substring(7);
      let event: NewEvents = {
        attachements: [{ key: keyattach, title:"", content: url}],
        title: titulo,
        text: text,
        key: user_id,
        user_id: this.userProv.user_profile.uid,
        created_at: date.getDate()+"/"+date.getMonth(),
        user_avatar : this.userProv.user_profile.avatar,
        user_name : this.userProv.user_profile.username,
        comments : 0,
        likes : 0
      };
      console.log(JSON.stringify(event));
      this.afDb.object(`/events/${user_id}`).update(event).then(
        (ok)=> {
          console.log("se subio bien!!!!!e!!!!!!!");
          console.log(JSON.stringify(ok));

        } ,(err)=>{
        console.warn("se subio mal events.......");
        console.warn(JSON.stringify(err));
      });
      //this.events.push( post );
      this.events.unshift( event );
      this.events = this.removeDuplicates(this.events, "key");
    }

    showToast( mensaje:string ){

      this.toastCtrl.create({
        message: mensaje,
        duration: 2000
      }).present();

    }

    removeDuplicates(originalArray, prop) {
       let newArray = [];
       let lookupObject  = {};

       for(var i in originalArray) {
          lookupObject[originalArray[i][prop]] = originalArray[i];
       }

       for(i in lookupObject) {
           newArray.push(lookupObject[i]);
       }
        return newArray;
    }
/*
    getPostId(){
       let d = new Date();
       return d.getFullYear()+""+d.getMonth().toString().padStart(2, '0')+""
              +d.getDate().toString().padStart(2,'0')+""+d.getHours().toString().padStart(2,'0')+""
              +d.getMinutes().toString().padStart(2,'0')+''+d.getSeconds().toString().padStart(2,'0')
              +d.getMilliseconds().toString().padStart(3,'0');
    }*/

  }

  interface NewEvents {
    created_at:string;
    attachements?:[object];
    uri_attachement?:string;
    user_address?:string;
    user_name?:string;
    user_avatar?:string;
    user_id?:string;
    title?:string;
    subtitle?:string;
    post_type?:string;
    text?:string;
    is_liked?:number;
    comments?:number;
    likes?:number;
    updated_at?:string;
    show?:string;
    type?:string;
    key?:string;
  }
