import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
//firebase
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
import 'firebase/auth';
import   "rxjs/add/operator/map";
import { Observable } from 'rxjs';


@Injectable()
export class CargaLugaresProvider {
  places:any[] = [];
  places_: Observable<any[]>;
  lastKey: string = null;
  constructor(public toastCtrl: ToastController,
               public afDb: AngularFireDatabase,
               public http: Http) {
                 //this.cargarTodosLugares();
                 /*this.cargarUltimolugar()
                     .subscribe( (post:any)=>{
                       if(post){
                          this.cargarLugares();
                       }
                  });*/
  }

  filterItems(searchTerm){
  return this.places.filter((place) => {
   return place.username.toLowerCase().indexOf(
     searchTerm.toLowerCase()) > -1;
   });
  }
  /*
  //cunado se busca por mas de una caracteristica
  filterItems(searchTerm){
   return this.items.filter((item) => {
    return item.title.toLowerCase().
     indexOf(searchTerm.toLowerCase()) > -1 ||
       item.description.toLowerCase().
        indexOf(searchTerm.toLowerCase()) > -1;
   });
  }
  */

  cargarTodosLugares(){
    //return this.afDb.list('places');
    return new Promise( (resolve, reject)=>{
        this.afDb.list('/users', ref=> ref.orderByChild('enable_support')
            .equalTo(true)).valueChanges().subscribe( (places:any)=>{
              if(places.length == 0){
                console.log("lugares--sin registros");
                resolve(false);
                return;
              }
              let numplace = places.length;
              let numplaceStorage = (this.places.length > 0) ? this.places.length -1 : 0;
              if( numplace > numplaceStorage ){
                for(let i = places.length-1; i >= 0; i-- ){
                  let place = places[i];
                  let canSave = false;
                  if(this.places.length > 0){
                    for( let j = 0; j <= this.places.length-1; j++ ){
                      if( place.uid == this.places[j].uid ){
                        canSave = true;
                      }
                    }
                    if(!canSave){
                      this.places.push(place);
                    }
                  }else{
                    this.places.push(place);
                  }
                }
              }else{
                this.places = [];
                for(let i = places.length-1; i >= 0; i-- ){
                  let place = places[i];
                  this.places.push(place);
                }
              }
              resolve(this.places);
            });
        });
  }
  cargarTodosLugares2(){
    return this.places_ = this.afDb.list('/users', ref=> ref.orderByChild('enable_support')
            .equalTo(true)).valueChanges();
  }

  cargarUltimolugar(){
    return this.afDb.list('/places', ref=> ref.orderByKey().limitToLast(1) )
            .valueChanges()
            .map( (place:any ) =>{
              if(place.length == 0){
                console.log("sin registros");
                return false;
              }
              this.lastKey = place[0].key;
              this.places.push( place[0] );
            });
  }

  cargarLugares(){
    return new Promise( (resolve, reject)=>{
      this.afDb.list('/places', ref => ref.limitToLast(4)
          .orderByKey()
          .endAt(this.lastKey) ).valueChanges()
          .subscribe( (places:any)=>{
            places.pop();
            if(places.length == 0){
              console.log("sin registros");
              resolve(false);
              return;
            }
            this.lastKey = places[0].key;
            for(let i = places.length-1; i >= 0; i-- ){
              let place = places[i];
              this.places.push(place);
            }
            resolve(true);
          });
    });
  }

}
