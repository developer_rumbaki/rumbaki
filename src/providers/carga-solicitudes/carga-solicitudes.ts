import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
//firebase
import { AngularFireDatabase } from 'angularfire2/database';
//import * as firebase from "firebase";
import   "rxjs/add/operator/map";
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
import 'firebase/auth';

import { Observable } from 'rxjs';

@Injectable()
export class CargaSolicitudesProvider {
  lastKey: string = null;
  requests_support: any[] =[];
  _requests_support: Observable<any[]>;

  constructor(public toastCtrl: ToastController,
                public afDb: AngularFireDatabase ) {
  /*console.log('Hello CargaUsersProvider Provider');
    this.cargarUltimaSolicitud().subscribe( ()=>{
      this.cargarSolicitudes()
    });*/
    this._requests_support = this.afDb.list('/users', ref=> ref.orderByChild('request_support')
            .equalTo(true) )
            .valueChanges();
  }

  habilitaUsuario(user){
    var updates = {};
    updates['/users/' + user.uid+'/'] = user;
    firebase.database().ref().update(updates);

  }

  private cargarUltimaSolicitud(){
    return this.afDb.list('/users', ref=> ref.orderByChild('request_support')
            .equalTo(true) )
            .valueChanges()
            .map( (users:any ) =>{
              console.log("key-------------");
              console.log(users);
              this.lastKey = users[0].key;
              this.requests_support.push( users[0] );
            });
  }

  cargarSolicitudes(){
      return new Promise( (resolve, reject)=>{
        this.afDb.list('/users', ref => ref.limitToLast(5)
                        .orderByKey()
                        .endAt(this.lastKey)
                        .orderByChild('request_support')
                        .equalTo(true)
                          ).valueChanges()
                        .subscribe( (users:any)=>{
                          users.pop();
                          if(users.length == 0){
                            console.log("sin registros");
                            resolve(false);
                            return;
                          }
                          this.lastKey = users[0].key;
                          for(let i = users.length-1; i >= 0; i-- ){
                            let user = users[i];
                            this.requests_support.push(user);
                          }
                          resolve(true);
                        });
      });
    }

    /*
    cargarSolicitudes(){
        return new Promise( (resolve, reject)=>{
          this.afDb.list('/users', ref=> ref.orderByChild('request_support')
          .equalTo(true) )
          .valueChanges()
          .map( (users:any ) =>{
            console.log("key-------------");
            console.log(users);
            //this.requests_support.push( users[0] );
            if(users.length == 0){
              console.log("sin registros");users
              resolve(false);
              return;
            }
            for(let i = users.length-1; i >= 0; i-- ){
              let user = users[i];
              this.requests_support.push(user);
            }
            resolve(true);
          });

        });


    }
    */

  }
