import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
//firebase
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/database';
import 'firebase/auth';
import   "rxjs/add/operator/map";

@Injectable()
export class CargaUsersProvider {

  users:any[] = [];
  lastKey: string = null;

  constructor(public toastCtrl: ToastController,
                public afDb: AngularFireDatabase ) {
    console.log('Hello CargaUsersProvider Provider');
    this.cargarUltimoUser().subscribe( ()=>{
      this.cargarTodosUsuarios()
    });
  }
  // const eightRef = rootRef.child('users').orderByChild('age_location').equalTo('28_Berlin');
  private cargarUltimoUser(){
    return this.afDb.list('/users', ref=> ref.limitToLast(1)
                                  //.orderByKey()
                                  .orderByChild('enable_support').equalTo(true) )
            .valueChanges()
            .map( (users:any ) =>{
              console.log("key-------------");
              console.log(users);
              this.lastKey = users[0].key;
              this.users.push( users[0] );
            });
  }

  cargarTodosUsuarios(){
    return new Promise( (resolve, reject)=>{
      this.afDb.list('/users', ref => ref.limitToLast(5)
                      //.orderByKey()
                      //.endAt(this.lastKey)
                      .orderByChild('enable_support')
                      .equalTo(true)
                        ).valueChanges()
                      .subscribe( (users:any)=>{
                        users.pop();
                        if(users.length == 0){
                          console.log("sin registros");
                          resolve(false);
                          return;
                        }
                        this.lastKey = users[0].key;
                        for(let i = users.length-1; i >= 0; i-- ){
                          let user = users[i];
                          this.users.push(user);
                        }
                        resolve(true);
                      });
    });
  }

}
