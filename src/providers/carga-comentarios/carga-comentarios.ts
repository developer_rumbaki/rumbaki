import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
//firebase
import { AngularFireDatabase } from 'angularfire2/database';
import   "rxjs/add/operator/map";
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';


//import { firebaseConfig } from '../../config/firebase.config';
//import { UserProvider } from '../../providers/user/user';

@Injectable()
export class CargaComentariosProvider {

  comments:any[] = [];
  lastKey:string = null;
  pid:string = null;
  constructor(public toastCtrl: ToastController,
               public afDb: AngularFireDatabase,
             /*public userProv: UserProvider */) {
    console.log('Hello CargaComentariosProvider Provider');

  }

  loadCommentsFstime(pid){
    //this.pid = pid;
    this.comments = [];
    console.log("id del post: "+pid);
    this.loadLastComment(pid)
        .subscribe( (comment:any)=>{
            this.loadComments(pid).then((ok)=>{
                console.warn("ok: "+JSON.stringify(ok));
            }).catch((err)=>{
              console.warn("error: "+JSON.stringify(err));
            });

         });
  }

  private loadLastComment(pid){
    return this.afDb.list(`/comments/${pid}`, ref=> ref.orderByKey().limitToLast(1) )
            .valueChanges()
            .map( (comments:any ) =>{
              try{
                this.lastKey = comments[0].key;
                this.comments.push( comments[0] );
              }catch(err){
                console.warn("error comments: "+JSON.stringify(err));
              }
            });
  }

  loadComments(pid){
    return new Promise( (resolve, reject)=>{
      this.afDb.list(`/comments/${pid}`, ref => ref.limitToLast(7)
          .orderByKey()
          .endAt(this.lastKey) ).valueChanges()
          .subscribe( (comments:any)=>{
            comments.pop();
            if(comments.length == 0){
              console.log("sin registros");
              resolve(false);
              return;
            }
            this.lastKey = comments[0].key;
            for(let i = comments.length-1; i >= 0; i-- ){
              let comment = comments[i];
              //this.comments.push(comment);
              this.comments.unshift(comment);
              this.comments = this.removeDuplicates(this.comments, "key");
            }
            resolve(true);
          });
    });
  }

  newComment( pid:string, uid:string, text:string, user_avatar:string, username:string ){
    let date = new Date();
    let key = new Date().getTime().toString();
    let comment = {
      pid: pid,
      uid: uid,
      key: key,
      created_at: date.getDate()+"/"+date.getMonth(),
      user_avatar : user_avatar,
      username : username,
      content : text
    };

    console.log(JSON.stringify(comment));
    this.afDb.object(`/comments/${pid}/${key}`).update(comment).then(
      (ok)=> {
        console.log("comment ok: "+JSON.stringify(ok));
      } ,(err)=>{
      console.log("coment fallo: "+JSON.stringify(err));
    });
    //this.posts.push( post );
    //this.comments = [];
    //this.loadCommentsFstime(pid);

    //this.comments.push( comment );
    this.comments = this.removeDuplicates(this.comments, "key");
  }

  showToast( mensaje:string ){

    this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    }).present();

  }

  removeDuplicates(originalArray, prop) {
     let newArray = [];
     let lookupObject  = {};

     for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
     }

     for(i in lookupObject) {
         newArray.push(lookupObject[i]);
     }
      return newArray;
  }

}
