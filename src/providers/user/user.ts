//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
//firebase
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from "firebase";
import   "rxjs/add/operator/map";
import { Observable } from 'rxjs';
/*
//asignar id forma 1
let userKey = this.afDb.list('/users').push(user).key;
this.afDb.list('/users').update( userKey,{key:userKey});

//asignar id forma 2
this.afDb.object(`/users/${user.uid}`).update(user).then(
()=>console.log("Usuario creado!"),
(err)=>{
console.log("se subio mal.......");
console.log(JSON.stringify(err));
});
*/
@Injectable()
export class UserProvider {

  usuario:Credenciales = {};
  //user: Observable<firebase.User>;
  user_profile:any;
  session_login = false;
  session_admin = false;
  rootRef: any;

  constructor( public afDb: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    public platform:Platform,
    public storage: Storage) {
      //this.logOut();

    }

    loginUser( email, password ){
      let platform = this.platform.is( "cordova" );
      let strorage = this.storage;
      return new Promise( (resolve, reject)=>{
        this.afAuth.auth.signInWithEmailAndPassword(email, password).then(( userdata:any ) => {
          if (userdata) {
            firebase.database().ref('/users/' + userdata.user.uid).once('value').then(function(snapshot) {
              if( platform ){//dispositivo
                strorage.ready().then( ()=>{
                    strorage.set( "user", { "profile" : snapshot.val(), "session":true });
                  });
                }else{//escritorio
                  localStorage.setItem( "user",  JSON.stringify( { "profile" : snapshot.val(), "session":true } ) );
                }
            })
            resolve(true);
          }else{
            resolve(false);
          }
        }, (err) => {
          reject(err);
        });
      });
    }

    loginUser2( email, password ){
      return new Promise( (resolve, reject)=>{
        this.afAuth.auth.signInWithEmailAndPassword(email, password).then(( userdata:any ) => {
          if (userdata) {
            firebase.database().ref('/users/' + userdata.user.uid).once('value').then(function(snapshot) {
              resolve(snapshot.val());
            })
          }else{
            reject();
          }
        }, (err) => {
          reject(err);
        });
      });
    }

    logOut(){
      return new Promise( (resolve, reject)=>{
        firebase.auth().signOut().
        then( ()=>{
          console.log("sesion cerrada");
          this.saveUserStorage( null, false );
          resolve(true);
        },(error)=>{
          reject();
        });
      });
    }

    verifyAuth(){
      return new Promise( (resolve, reject)=>{
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
              resolve({ "profile":snapshot.val(), "auth_afb": user, session:true });
            });
          }
          else{
            firebase.auth().signOut()
            resolve({ "profile":null, "auth_afb": null, session:false });
          }
        });
      });
    }

    loadUserStorage(){
      return new Promise( (resolve, reject)=>{
        if( this.platform.is("cordova") ){
          //dispositivo
          this.storage.ready().then( ()=>{
          this.storage.get("user").then( (resp_user)=>{
            console.log("load storage device: "+resp_user.session);
            if( resp_user ){
              this.user_profile = resp_user.profile;
              this.session_login = resp_user.session;
              this.session_admin = ((resp_user.profile)? resp_user.profile.enable_admin: false);
              resolve(resp_user);
            }else{
              this.user_profile = false;
              this.session_login = false;
              this.session_admin = false;
              resolve(false);
            }
          });
        });
      }else{
        //escritorio
        let usStore = localStorage.getItem( "user" );
        if( usStore ){
          let _usStore = JSON.parse( usStore );
          console.log("load storage desktop: "+_usStore.session);
          this.user_profile = _usStore.profile;
          this.session_login = _usStore.session;
          this.session_admin = ((_usStore.profile)? _usStore.profile.enable_admin: false);
          resolve(true);
        }
        else{
          this.user_profile = false;
          this.session_login = false;
          this.session_admin = false;
          resolve(false);
        }
      }
    });

  }

  saveUserStorage( profile, session ){
    if( this.platform.is( "cordova" ) ){//dispositivo
      this.storage.ready().then( ()=>{
          this.storage.set( "user", { "profile" : profile, "session":session });
        });
      }else{//escritorio
        localStorage.setItem( "user",  JSON.stringify( { "profile" : profile, "session":session } ) );
      }
      this.user_profile = profile;
      this.session_login = session;
      this.session_admin = ( profile && profile.enable_admin ) || false ;
    }

updateUser( user, updtImg ){
  return new Promise( (resolve, reject)=>{
    if( updtImg ){
      let storeRef = firebase.storage().ref();
      let avatar_id:string = new Date().valueOf().toString();
      let uploadTask : firebase.storage.UploadTask =
      storeRef.child(`avatar/${avatar_id}`).putString( user.avatar, 'base64', { contentType: 'image/jpeg'} );
      uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
        ()=>{ },//porcentaje de archivo subido
        (error)=>{
          //error de carga
          console.log( "Error en la carga" );
          reject(error);
        },
        ( )=>{
          console.log( "Avatar subido" );
          uploadTask.snapshot.ref.getDownloadURL().then(downloadAvatarURL => {
            user.avatar = downloadAvatarURL;
            //inicio de condicion de support
            firebase.auth().currentUser.updateProfile({
              displayName: user.username,
              photoURL: downloadAvatarURL
            }).then(function() {
              // Update successful.
            }).catch(function(error) {
              // An error happened.
            });
            this.saveUserStorage( user, true );
            this.afDb.object(`/users/${user.uid}`).update(user)
            .then(()=>{
              resolve(true);
            })
            .catch(()=>{
              reject()
            });

          });
        });
      }
      else{
        this.saveUserStorage( user, true );
        this.afDb.object(`/users/${user.uid}`).update(user).then(
          ()=>{
            console.log("Usuario creado!")
            //user = user;
            //this.saveUserStorage();
            resolve(true);
          },
          (err)=>{
            console.log("se subio mal.......");
            resolve(false);
          });
        }

      });
    }

    createNewUser( user ){
      return new Promise( (resolve, reject)=>{
        //si el provider es facebook
        if(user.provider == "facebook")
        {

          this.afDb.object(`/users/${user.uid}`).update(user).then(
            ()=>{
              console.log("Usuario creado!")
              //user = user;
              //this.saveUserStorage();
              resolve(true);
            },
            (err)=>{
              console.log("se subio mal.......");
              resolve(false);
            });
            this.afDb.object(`/places/${user.uid}`).update(user).then(
              ()=>{
                console.log("Usuario creado!")
                resolve(true);
              },
              (err)=>{
                console.log("se subio mal.......");
                resolve(false);
              });
            }
            //si el provider es google
            else if( user.provider == "google"){
              console.log("google log");
              //this.saveUserStorage();
            }
            //si el provider es email
            else if( user.provider == "email"){
              //correccion de inicio de session
              let storeRef = firebase.storage().ref();
              firebase.auth().createUserWithEmailAndPassword( user.email, user.password )
              .then( ( userData )=>{
                console.log("Usuario creado.");
                let _user = userData.user;
                user.uid = _user.uid;
                user.key = _user.uid;
                if( user.avatar == "" ){
                  this.saveUserStorage( user, true );
                  this.afDb.object(`/users/${user.uid}`).update(user).then(()=>{
                    resolve(user);
                  }).catch((err)=>reject(err));
                }
                //carga de imagen
                else{
                  let avatar_id:string = new Date().valueOf().toString();
                  let uploadTask : firebase.storage.UploadTask =
                  storeRef.child(`avatar/${avatar_id}`).putString( user.avatar, 'base64', { contentType: 'image/jpeg'} );
                  uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
                    ()=>{ },//porcentaje de archivo subido
                    (error)=>{
                      //error de carga
                      console.log( "Error en la carga" );
                      reject(error);
                    },
                    ( )=>{
                      console.log( "Avatar subido" );
                      uploadTask.snapshot.ref.getDownloadURL().then(downloadAvatarURL => {
                        user.avatar = downloadAvatarURL;
                        //inicio de condicion de support
                        firebase.auth().currentUser.updateProfile({
                          displayName: user.username,
                          photoURL: downloadAvatarURL
                        }).then(function() {
                          // Update successful.
                        }).catch(function(error) {
                          // An error happened.
                        });
                        this.saveUserStorage( user, true );
                        this.afDb.object(`/users/${user.uid}`).update(user)
                        .then(()=>{
                          resolve(true);
                        })
                        .catch(()=>{
                          reject()
                        });

                      });
                    });
                  }
                  resolve(true);
                }).catch( (error)=>{
                  console.log("error al crear usuario..");
                  reject(error);
                });
                //fin de correccion de inicio de session
              }
              //end promise
            });
          }

    resetPassword(email){
      return new Promise( ( resolve, reject )=>{
        console.log("Email:" + email);
        firebase.auth().sendPasswordResetEmail( email ).
        then((user) => {
          // this.user = user;
          // this.saveUserStorage();
          resolve();
        }, (error) => {
          reject(error);
        });
      });
    }

    changePassword(oldPassword, newPassword){
      return new Promise( ( resolve, reject )=>{
        console.log("Email:" + oldPassword+ " "+ newPassword);
        firebase.auth().confirmPasswordReset(oldPassword, newPassword).
        then((user) => {
          console.log("password cambiada-------");
          // this.user = user;
          // this.saveUserStorage();
          resolve();
        }, (error) => {
          reject(error);
        });
      });
    }

    searchUser(uid){
      return new Promise( (resolve, reject)=>{
          if (uid) {
            firebase.database().ref('/users/' + uid).once('value').then(function(snapshot) {
              if(snapshot.val()){
                resolve(snapshot.val());
              }else{
                reject(false);
              }
            });
          }
          else{
            reject(false);
          }
      });
    }
    //end class
  }

        export interface UserApp {
          avatar:string;
          username:string;
          firstname:string;
          lastname:string;
          email:string;
          groups: {
            group_admin:boolean;
            group_anonym:boolean;
            group_support:boolean;
            group_users:boolean;
          };
          key?:string;
          password?:string;
          uid?:string;
          address?:string;
          url?:string;
          enable?:boolean;
          enable_support?:boolean;
          request_support?:boolean;
          enable_admin?:boolean;
          location?: {
            lat?:string;
            lng?:string;
          };
          created_at?:string;
          provider?:string;
        }

        export interface Credenciales{
          provider?:string;
          nombre?:string;
          imgUrl?:string;
          email?:string;
          uid?:string;
        }
