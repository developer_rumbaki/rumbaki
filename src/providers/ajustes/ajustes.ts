//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
//para detectar el dispositivo en donde se esta ejecutando
import { Platform } from 'ionic-angular';

import firebase from 'firebase/app';
import 'firebase/auth';



@Injectable()
export class AjustesProvider {

  ajustes = {
    mostrar_slides:true,
    provider:"",
    session:false,
    uid:"",
    key:""
  }
  constructor(/*public http: HttpClient,*/
              private platform:Platform,
              private storage: Storage) {
    console.log('Hello AjustesProvider Provider');
  }

  cargarStorage(){
    //leer ajustes
    return new Promise( (resolve, reject)=>{
        if( this.platform.is("cordova") ){
          //dispositivo
          console.log("iniciando storage");
          this.storage.ready().then( ()=>{
            console.log("storage listo");
            this.storage.get("ajustes").then( (resp_ajustes)=>{
              if( resp_ajustes ){
                this.ajustes = resp_ajustes;
              }
              resolve();
            });
          });
        }else{
          //escritorio
          if( localStorage.getItem( "ajustes" ) ){
          this.ajustes = JSON.parse( localStorage.getItem( "ajustes" ) );
        }
        resolve();
      }
    });

  }
  //guardar ajustes
  guardarStorage(){
    if( this.platform.is( "cordova" ) ){
      //dispositivo
      this.storage.ready().then( ()=>{
        this.storage.set("ajustes", this.ajustes );
      });
    }else{
      //escritorio
      localStorage.setItem( "ajustes", JSON.stringify( this.ajustes ) );
    }
  }

}
